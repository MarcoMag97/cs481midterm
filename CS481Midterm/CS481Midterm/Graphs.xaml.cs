﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microcharts;
using Entry = Microcharts.Entry;
using System.Net.Http;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Xamarin.Forms.Xaml;
using SkiaSharp;

namespace CS481Midterm
{

    public partial class Graphs : ContentPage
    {
        int count;
        public Graphs()
        {
            InitializeComponent();
            CreateChart();
        }

        private void CreateChart()
        {
            var entry = new List<Entry>();
            if (List.myStockModel != null)
            {
                foreach (var stockx in List.myStockModel.TimeSeriesDaily.Values)
                {
                    count = count + 1;
                    if (count % 8 == 0)
                    {


                        var createnew = new Entry(float.Parse(stockx.The2High))
                        {
                            Color = SKColor.Parse("0A71DF"),
                            ValueLabel = stockx.The2High,
                        };
                        entry.Add(createnew);
                        entry.Reverse();
                    }
                }
            }
            var linechart = new LineChart()
            {
                Entries = entry
            };
            StockChart.Chart = linechart;
        }


        void ContentPage_Appearing(object sender, EventArgs e)
        {
            var idk = List.myStockModel;
            CreateChart();
        }
    }
}