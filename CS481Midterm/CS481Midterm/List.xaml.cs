﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481Midterm
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class List : ContentPage
    {
        public ObservableCollection<string> Items { get; set; }
        ObservableCollection<TimeSeriesDaily> myStocksCollection = new ObservableCollection<TimeSeriesDaily>();
        public static StockModel myStockModel;

        public List()
        {
            InitializeComponent();

   //         Items = new ObservableCollection<string>
   //         {
   //             "Item 1",

   //         };
            
			
			//MyListView.ItemsSource = Items;
            
        }

        async void SearchAPIButtonClicked(object sender, ItemTappedEventArgs e)
        {
            var client = new HttpClient();
            string stockApiAddress = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + userEntry.Text + "&apikey=K0DGV080FNXJ6FGL";
          
            var uri = new Uri(stockApiAddress);

            myStockModel = new StockModel();
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
                myStockModel = JsonConvert.DeserializeObject<StockModel>(jsonContent);
            }

            MyListView.ItemsSource = myStockModel.TimeSeriesDaily;//new ObservableCollection<Person>(spaceData.People);
            App.X = myStockModel.TimeSeriesDaily;
            if (myStockModel.TimeSeriesDaily!=null)
            {
                highLabel.Text = myStockModel.TimeSeriesDaily.Values.Max(x => x.The2High);
                lowLabel.Text = myStockModel.TimeSeriesDaily.Values.Min(x => x.The3Low);
            }
            if(myStockModel.MetaData == null)
            {
                await DisplayAlert("Error", "Invalid Symbol", "Close");
            }
        }
        //{
        //    if (e.Item == null)
        //        return;

        //    await DisplayAlert("Item Tapped", "An item was tapped.", "OK");

        //    //Deselect Item
        //    ((ListView)sender).SelectedItem = null;
        //}

       /* private async void SearchAPIButtonClicked(object sender, EventArgs e)
        {
            if (userEntry.Text != "" && userEntry.Text != null)
            {
                HttpClient client = new HttpClient();
                string stockApiAddress = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + userEntry.Text + "&apikey=K0DGV080FNXJ6FGL";
                Uri stockURI = new Uri(stockApiAddress);
                HttpResponseMessage response = await client.GetAsync(stockApiAddress);

                //if (response.IsSuccessStatusCode)
                //{
                //    string jsonString = await response.Content.ReadAsStringAsync();
                //    StockModel myStockModel = StockModel.FromJson(jsonString);

                //    myStocksCollection.Clear();
                //    //myStockModel.TimeSeriesDaily.ForEach(myStocksCollection.Add);

                //    for (int i = 0; i < myStockModel.TimeSeriesDaily.Count; i++)
                //    {
                //        myStocksCollection.Add(myStockModel.TimeSeriesDaily.OnDeserialization);
                //    }
                //    //MyListView.ItemsSource = myStocksCollection;
                //}
            }
        }*/
    }
}
